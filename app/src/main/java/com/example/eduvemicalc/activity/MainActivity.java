package com.example.eduvemicalc.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.evrencoskun.tableview.TableView;
import com.example.eduvemicalc.R;
import com.example.eduvemicalc.adapter.MyTableViewAdapter;
import com.example.eduvemicalc.model.Cell;
import com.example.eduvemicalc.model.ColumnHeader;
import com.example.eduvemicalc.model.RowHeader;
import com.example.eduvemicalc.utils.Utils;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.example.eduvemicalc.utils.Utils.fillEmptyList;
import static com.example.eduvemicalc.utils.Utils.parth;
import static com.example.eduvemicalc.utils.Utils.toDouble;
import static com.example.eduvemicalc.utils.Utils.toFloat;
import static com.example.eduvemicalc.utils.Utils.toInt;
import static com.example.eduvemicalc.utils.Utils.valueOfPercentage;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    final float GST_RATE = 0.18f;
    LinearLayout layoutResult, layoutTable;
    ScrollView scrollView;
    EditText edtLnAmt,edtInttRate, edtSubvention, edtTranches;
    TextView edtDate ,txtProcFeeAmt, txtLoanTerm;
    Spinner spRepaymntCycle, spProcFee, spFeePaymentInterval, spPaymentInterval;
    RadioGroup rg_paymnt_mode;
    Button btnCalculate;
    TextView btnAmortTable;
    ExpandableLayout expandableLayout;
    double loanAmt=0.0, inttRate=0.0, procFee=0.0;
    String subvention="0.0", tranches="1", feePaymentInterval="", paymentInterval="", loanTerm="1", dateOfPaymentOrDisburse="",
    loanRepaymentRecycle="0", paymentMode ="",balloonPayment="0";

    String cellJ5 = "", cellJ12="", cellJ9= "", cellF17 = "", cellJ10 = "";

    //LOOKUP TABLE 1
    ArrayList<String> col_O3_O10_List = new ArrayList<>();
    ArrayList<String> col_P3_P10_List = new ArrayList<>();
    ArrayList<String> col_Q3_Q10_List = new ArrayList<>();
    ArrayList<String> col_R3_R10_List = new ArrayList<>();
    ArrayList<String> col_S3_S10_List = new ArrayList<>();

    //LOOKUP TBALE 2
    ArrayList<String> col_O12_O18_List = new ArrayList<>();
    ArrayList<String> col_P12_P18_List = new ArrayList<>();
    ArrayList<String> col_Q12_Q18_List = new ArrayList<>();
    ArrayList<String> col_R12_R14_List = new ArrayList<>();
    ArrayList<String> col_S12_S14_List = new ArrayList<>();

    //amort table
    ArrayList<String> col_C29_C100_List = new ArrayList<>();  //Date
    ArrayList<String> col_D29_D100_List = new ArrayList<>();  //Month
    ArrayList<String> col_E29_E100_List = new ArrayList<>();    //Principal
    ArrayList<String> col_F29_F100_List = new ArrayList<>();    //Interest
    ArrayList<String> col_G29_G100_List = new ArrayList<>();    //EMI
    ArrayList<String> col_H29_H100_List = new ArrayList<>();    //Balance

    //OUTPUT
    TextView txtFixedRateOfIntt, txtNxtEMIDue, txtTotToBeDisbursed, txtGst,txtTotalProcessingFee
            , txtEmiAmt, txtDtofNxtPaymnt, txtTotAmtPaid;


    private List<RowHeader> mRowHeaderList;
    private List<ColumnHeader> mColumnHeaderList;
    private List<List<Cell>> mCellList;
    TableView tableView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setToolbar();
        initLookUpTables();
        setTermSpinners();

    }

    private void setToolbar() {
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Launched")
                .putContentType(getString(R.string.app_name)));

        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reset){
            layoutResult.setVisibility(View.GONE);
            layoutTable.setVisibility(View.GONE);
            edtLnAmt.setText("");
            edtInttRate.setText("");
            edtDate.setText("");
        }
        return true;
    }

    private void initLookUpTables() {
        fillEmptyList(col_O3_O10_List, 0,2);
        col_O3_O10_List.add(3,"Monthly");
        col_O3_O10_List.add(4,"Bi-Monthly");
        col_O3_O10_List.add(5,"Quarterly");
        col_O3_O10_List.add(6,"Semi-Annually");
        col_O3_O10_List.add(7,"Annually");
        col_O3_O10_List.add(8,"1");
        col_O3_O10_List.add(9,"");
        col_O3_O10_List.add(10,"");

        fillEmptyList(col_P3_P10_List, 0,2);
        col_P3_P10_List.add(3,"12");
        col_P3_P10_List.add(4,"6");
        col_P3_P10_List.add(5,"4");
        col_P3_P10_List.add(6,"2");
        col_P3_P10_List.add(7,"1");
        col_P3_P10_List.add(8,"");
        col_P3_P10_List.add(9,"");
        col_P3_P10_List.add(10,"");

        fillEmptyList(col_Q3_Q10_List, 0,2);
        col_Q3_Q10_List.add(3,"1");
        col_Q3_Q10_List.add(4,"2");
        col_Q3_Q10_List.add(5,"3");
        col_Q3_Q10_List.add(6,"6");
        col_Q3_Q10_List.add(7,"12");
        col_Q3_Q10_List.add(8,"");
        col_Q3_Q10_List.add(9,"1");
        col_Q3_Q10_List.add(10,"");

        fillEmptyList(col_R3_R10_List, 0,2);
        col_R3_R10_List.add(3,"0");
        col_R3_R10_List.add(4,"1");
        col_R3_R10_List.add(5,"2");
        col_R3_R10_List.add(6,"3");
        col_R3_R10_List.add(7,"4");
        col_R3_R10_List.add(8,"5");
        col_R3_R10_List.add(9,"6");
        col_R3_R10_List.add(10,"7");

        fillEmptyList(col_S3_S10_List, 0,2);
        col_S3_S10_List.add(3,"");
        col_S3_S10_List.add(4,"");
        col_S3_S10_List.add(5,"");
        col_S3_S10_List.add(6,"");
        col_S3_S10_List.add(7,"");
        col_S3_S10_List.add(8,"");
        col_S3_S10_List.add(9,"");
        col_S3_S10_List.add(10,"");


        fillEmptyList(col_O12_O18_List, 0,11);
        col_O12_O18_List.add(12,"Loan Amount:");
        col_O12_O18_List.add(13,"Subvention (%)");
        col_O12_O18_List.add(14,"Interest Rate (Reducing Rate of Interest)");
        col_O12_O18_List.add(15,"Payment Interval");
        col_O12_O18_List.add(16,"Loan Term in Years");
        col_O12_O18_List.add(17,"Payments in Advance / Arrears");
        col_O12_O18_List.add(18,"Check-Up:");

        fillEmptyList(col_Q12_Q18_List, 0,11);
        col_Q12_Q18_List.add(12,"");
        col_Q12_Q18_List.add(13,"");
        col_Q12_Q18_List.add(14,"");
        col_Q12_Q18_List.add(15,"");
        col_Q12_Q18_List.add(16,"");
        col_Q12_Q18_List.add(17,"");
        col_Q12_Q18_List.add(18,"");

        fillEmptyList(col_R12_R14_List, 0,11);
        col_R12_R14_List.add(12,"0");
        col_R12_R14_List.add(13,"1");
        col_R12_R14_List.add(14,"");

        fillEmptyList(col_S12_S14_List, 0,11);
        col_S12_S14_List.add(12,"Arrears");
        col_S12_S14_List.add(13,"Advance");
        col_S12_S14_List.add(14,"");

        fillEmptyList(col_C29_C100_List, 0,199);
        fillEmptyList(col_D29_D100_List, 0,199);
        fillEmptyList(col_E29_E100_List, 0,199);
        fillEmptyList(col_F29_F100_List, 0,199);
        fillEmptyList(col_G29_G100_List, 0,199);
        fillEmptyList(col_H29_H100_List, 0,199);


    }

    private void setTermSpinners() {
//        String[] yrsArray = getResources().getStringArray(R.array.years);
//        ArrayAdapter<String> yrsAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, yrsArray);
//        spYr.setAdapter(yrsAdapter);

//        String[] mnthArray = getResources().getStringArray(R.array.mnths);
//        ArrayAdapter<String> mnhtsAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, mnthArray);
//        spMnth.setAdapter(mnhtsAdapter);

        String[] repaymntCycleArray = getResources().getStringArray(R.array.repaymnt_recycles);
        ArrayAdapter<String> repaymentAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, repaymntCycleArray);
        spRepaymntCycle.setAdapter(repaymentAdapter);


    }

    private void init() {

        layoutResult = findViewById(R.id.layoutResult);
        layoutTable = findViewById(R.id.layoutTable);
        scrollView = findViewById(R.id.scrollView);
        edtLnAmt = findViewById(R.id.edtLnAmt);
        edtInttRate = findViewById(R.id.edtInttRate);
        edtDate = findViewById(R.id.edtDate);
        edtSubvention = findViewById(R.id.edtSubvention);
        txtLoanTerm = findViewById(R.id.txtLoanTerm);
//        spYr = findViewById(R.id.spYr);
//        spMnth = findViewById(R.id.spMnth);
        spRepaymntCycle = findViewById(R.id.spRepaymntCycle);


        spFeePaymentInterval = findViewById(R.id.spFeePaymentInterval);
        String[] feePaymentIntervalArray = getResources().getStringArray(R.array.payment_interval);
        ArrayAdapter<String> feePaymentIntervalAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, feePaymentIntervalArray);
        spFeePaymentInterval.setAdapter(feePaymentIntervalAdapter);

        spPaymentInterval = findViewById(R.id.spPaymentInterval) ;
        spPaymentInterval.setAdapter(feePaymentIntervalAdapter);

        rg_paymnt_mode = findViewById(R.id.rg_paymnt_mode);
        btnCalculate = findViewById(R.id.btnCalculate);
        edtTranches = findViewById(R.id.edtTranches);

        spProcFee = findViewById(R.id.edtProcFee);
        String[] processingFeeArray = getResources().getStringArray(R.array.processing_fee_rates);
        ArrayAdapter<String> processingFeeAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, processingFeeArray);
        spProcFee.setAdapter(processingFeeAdapter);
        spProcFee.setSelection(1);

        txtProcFeeAmt = findViewById(R.id.txtProcFeeAmt);
        txtNxtEMIDue = findViewById(R.id.txtNxtEMIDue);
        txtTotToBeDisbursed = findViewById(R.id.txtTotToBeDisbursed);
        txtGst= findViewById(R.id.txtGst);
        txtTotalProcessingFee = findViewById(R.id.txtTotalProcessingFee);
        txtFixedRateOfIntt = findViewById(R.id.txtFixedRateOfIntt);
        txtEmiAmt = findViewById(R.id.txtEmiAmt);
        txtDtofNxtPaymnt = findViewById(R.id.txtDtofNxtPaymnt);
        txtTotAmtPaid = findViewById(R.id.txtTotAmtPaid);

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                DatePickerDialog pickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d(TAG,"Date- "+year+"-"+month+" "+dayOfMonth);
                        month = month+1;  //month start from 0
                        edtDate.setText(dayOfMonth+"/"+month+"/"+year);
                    }
                },cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
                pickerDialog.show();
            }
        });

        txtLoanTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getLayoutInflater().inflate(R.layout.layout_number_picker,null);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setView(view);
                builder.setCancelable(false);
                builder.setTitle("Select loan term");
                final NumberPicker numberPicker1 = view.findViewById(R.id.numberPick);
                final String  vals[] = new String[54];
                int j=0;
                for (int i=3; j<vals.length; i=i+3,j++){
                    vals[j] = String.valueOf(i);
                }
                numberPicker1.setDisplayedValues(vals);
                numberPicker1.setMinValue(0);
                numberPicker1.setMaxValue(vals.length-1);
                numberPicker1.setWrapSelectorWheel(true);
                numberPicker1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        Log.d("PICKER", "old - "+oldVal +"new - "+newVal);
                        loanTerm = vals[numberPicker1.getValue()];
                        txtLoanTerm.setText(loanTerm);
                        loanTerm = String.valueOf(toDouble(loanTerm)/12); //convert loan term in yrs
                    }
                });
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog =  builder.create();
                dialog.show();

            }
        });

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInputs();
            }
        });


        expandableLayout = findViewById(R.id.expandableLayout);
        tableView = findViewById(R.id.tableView);
        btnAmortTable = findViewById(R.id.btnAmortTable);
        btnAmortTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableLayout.toggle();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (expandableLayout.isExpanded()) {
                        prepareTableView();
                        btnAmortTable.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_chevron_circle_up_solid), null);
                    }else
                        btnAmortTable.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_chevron_circle_down_solid), null);
                }
            }

        });
        expandableLayout.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
            @Override
            public void onExpansionUpdate(float expansionFraction, int state) {
                if (state == 3){
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
//                        scrollView.smoothScrollTo(0,btnAmortTable.getTop());
                            scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });

                }

            }
        });

    }

    private void validateInputs() {

        if (edtLnAmt.getText().toString().equals("") || txtLoanTerm.getText().toString().equals("")
             || edtDate.getText().toString().equals("")) {
            if (edtLnAmt.getText().toString().equals(""))   edtLnAmt.setError("Enter loan amount");
            if (txtLoanTerm.getText().toString().equals("")) txtLoanTerm.setError("Enter loan term");
            if (edtDate.getText().toString().equals("")) edtDate.setError(" Please mention date");
            else edtDate.setError(null);
        }
        else{
            loanAmt = toFloat(edtLnAmt.getText().toString());
            if (edtSubvention.getText().toString().equals(""))
                subvention ="0";
            else
                subvention = edtSubvention.getText().toString();

            if (edtInttRate.getText().toString().equals(""))
                inttRate = 0;
            else
                inttRate = toFloat(edtInttRate.getText().toString());
            if (edtTranches.getVisibility() == View.VISIBLE && !edtTranches.getText().toString().equals(""))
                tranches = edtTranches.getText().toString();
            if (spFeePaymentInterval.getVisibility() == View.VISIBLE)
                feePaymentInterval = spFeePaymentInterval.getSelectedItem().toString();
            paymentInterval = spPaymentInterval.getSelectedItem().toString();

            loanTerm = String.valueOf(toDouble(txtLoanTerm.getText().toString())/12); //convert loan term in yrs


            if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance)
                paymentMode = "Advance";
            else paymentMode = "Arrears";
            dateOfPaymentOrDisburse = edtDate.getText().toString();
            loanRepaymentRecycle =spRepaymntCycle.getSelectedItem().toString();
            procFee = toFloat(spProcFee.getSelectedItem().toString());
            //validation code

            calculate();

        }

    }

    private void calculate() {


        prepareLookups();
        layoutResult.setVisibility(View.VISIBLE);
        layoutTable.setVisibility(View.VISIBLE);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0, layoutResult.getTop());
//                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
        /********************Output**************/
        //fixed rate of intt
        double fixedRateOfntt = toDouble(cellJ5)/loanAmt/(toDouble(loanTerm));
//        fixedRateOfntt = valueOfPercentage(fixedRateOfntt,1);
//        fixedRateOfntt = Math.round((float) fixedRateOfntt);
        txtFixedRateOfIntt.setText( String.format("%.2f", (fixedRateOfntt*100)).toString().concat(" %"));
//        txtFixedRateOfIntt.setText( String.valueOf(fixedRateOfntt).concat(" %"));

        //processing fee amt
        double processingFeeAmt = 0.0f;
        if (spProcFee.getSelectedItemPosition() != 0 && !edtLnAmt.getText().toString().equalsIgnoreCase("") ){
            loanAmt = toDouble(edtLnAmt.getText().toString());
            procFee = toDouble(spProcFee.getSelectedItem().toString());
            processingFeeAmt  = Utils.valueOfPercentage(procFee,loanAmt);
            txtProcFeeAmt.setText(String.valueOf(processingFeeAmt));
        }

        //next emi due
        String datePattern  = "dd/mm/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        try {
            Date  date = simpleDateFormat.parse(dateOfPaymentOrDisburse);
            int day= date.getDate();
            if (day < 21)
                txtNxtEMIDue.setText(getResources().getString(R.string.next_mnth));
            else txtNxtEMIDue.setText(getResources().getString(R.string.next_to_next_mnth));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //total amount to be disbursed
        double f4 = (1 - (valueOfPercentage(toDouble(subvention),1)));
        txtTotToBeDisbursed.setText( String.valueOf(loanAmt *  f4) );

        //GST
        double gst = GST_RATE * processingFeeAmt;
        txtGst.setText(String.valueOf(gst));

        //Total processing fee
        double totalProcessingFee = processingFeeAmt + gst;
        txtTotalProcessingFee.setText("\u20B9 "+String.format("%.2f",totalProcessingFee));

        //EMi amount
        txtEmiAmt.setText("\u20B9 "+String.valueOf(Math.ceil(toDouble(cellF17))));

        //Repayable Amount
        txtTotAmtPaid.setText("\u20B9 "+String.valueOf(Math.ceil(toDouble(cellF17)) * toDouble(col_P3_P10_List.get(9)) ));

        prepareAmortTable();

        if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance)
            txtDtofNxtPaymnt.setText(col_C29_C100_List.get(30));
        else
            txtDtofNxtPaymnt.setText(col_C29_C100_List.get(29));


    }

    private void prepareLookups() {
        col_P3_P10_List.set(8, lookForElement(col_O3_O10_List, col_P3_P10_List, spPaymentInterval.getSelectedItem().toString()));  //set P8 cell
        col_P3_P10_List.set(9, String.valueOf(toFloat(loanTerm) * toFloat(col_P3_P10_List.get(8) )) );
        col_Q3_Q10_List.set(8, lookForElement(col_Q3_Q10_List, col_P3_P10_List, col_P3_P10_List.get(8)));

        //R14
        if (paymentMode.equalsIgnoreCase("Advance"))
            col_R12_R14_List.set(14, "1");
        else col_R12_R14_List.set(14, "0");

        //Q12
        if (loanAmt == 0)
            col_Q12_Q18_List.set(12, "1");
        else {
            if (loanAmt < 0 || loanAmt <= toFloat(subvention)) col_Q12_Q18_List.set(12, "1");
            else             col_Q12_Q18_List.set(12, "0");
        }
        //Q13
        if (subvention.equalsIgnoreCase("")) col_Q12_Q18_List.set(13, "1");
        else {
            if ((valueOfPercentage(toFloat(subvention ),1))  < 0 || (valueOfPercentage(toFloat(subvention ),1)) > loanAmt ) col_Q12_Q18_List.set(13, "1");
            else col_Q12_Q18_List.set(13, "0");
        }
        //Q14
        if (inttRate <= 0 || valueOfPercentage(inttRate,1) >1 ) col_Q12_Q18_List.set(14, "1");
        else col_Q12_Q18_List.set(14, "0");

        //Q15
        col_Q12_Q18_List.set(15,"0");

        //Q16
        if (loanTerm.equals("") || toFloat(loanTerm)==0) col_Q12_Q18_List.set(16, "1");
        else col_Q12_Q18_List.set(16, "0");

        //Q17
        if (col_R12_R14_List.get(14).equals("")) col_Q12_Q18_List.set(17,"1");
        else             col_Q12_Q18_List.set(17,"0");

        int checkup = 0;
        for (int i=12; i<18; i++){
            checkup=checkup+ (Integer.parseInt(col_Q12_Q18_List.get(i)));
        }
        //Q18
        col_Q12_Q18_List.set(18,String.valueOf(checkup));

        //J12
        cellJ12 = String.valueOf(toFloat(loanTerm) * 12);
        col_O3_O10_List.set(9,cellJ12);

        //J5
        if (toFloat(col_Q12_Q18_List.get(18)) > 0){ //IF(Q18>0,"0",(O9-O8+1)*(((F2-F3)*F5/(1-(P8/(P8+F5))^(P8*F10))+F3*F5)/P8*IF(R14=1,P8/(P8+F5),1))-F2)
            cellJ5 = "0";
        } else {
            float O9 = toFloat(col_O3_O10_List.get(9));
            float O8 = toFloat(col_O3_O10_List.get(8));
            double F2 = loanAmt;
            float F3 = toFloat(balloonPayment);
            double F5 = valueOfPercentage(inttRate,1);
            float P8 = toFloat(col_P3_P10_List.get(8));
            float F10 = toFloat(loanTerm);
            float R14 = toFloat(col_R12_R14_List.get(14));
            double innerIfResult =  ((R14 == 1)? P8/(P8+F5) : 1); //IF(R14=1,P8/(P8+F5),1)

            double E1 = (O9-O8+1);
            double e1 = (F2-F3)*F5;
            double e2 = P8/(P8+F5);
            double e3=  (P8*F10);
            double expo = Math.pow( e2, e3 );
            double e4 = ((1 - expo) + F3*F5 );
            double e5 = P8 * innerIfResult;

            double E2 = (((F2-F3)*F5/(1-expo)+F3*F5)/P8* innerIfResult);
            double z = (E1 * E2) - F2;
            Log.d("ZZZZ",""+z);

            cellJ5 =String.valueOf( Math.round(z) );
//            cellJ5 =String.valueOf( (O9-O8+1)*(((F2-F3)*F5/(1- expo)+F3*F5)/P8* innerIfResult  )-F2 );

        }

//        F17  =    IF(Q18>0,F2/P9,ROUND(((F2-F3)*F5/(1-(P8/(P8+F5))^(P8*P9/P8))+F3*F5)/P8*P8/(P8+R14*F5),8))
        if (toFloat(col_Q12_Q18_List.get(18)) > 0){
            double F2 = loanAmt;
            float P9 = toFloat(col_P3_P10_List.get(9));
            cellF17 = String.valueOf((F2/P9));
        } else{
            double F2 =  loanAmt;
            float F3 = toFloat(balloonPayment);
            double F5 = valueOfPercentage(inttRate,1);
            float P8 = toFloat(col_P3_P10_List.get(8));
            float P9 = toFloat(col_P3_P10_List.get(9));
            float R14 = toFloat(col_R12_R14_List.get(14));

            double exp1 = (F2-F3)*F5;
            double exp2 = (P8/(P8+F5));
            float exp3 = (P8*P9/P8);
            double exp4 =  F3*F5;
            float exp5 = P8*P8;
            double exp6 = (P8+R14*F5);
            double expo =  Math.pow(exp2, exp3);

            double x=((F2-F3)*F5/(1-expo)+F3*F5)/P8*P8/(P8+R14*F5);
            cellF17 = String.valueOf(x);
        }

        //J10
        //IF(AND(F5=0,F11="Advance"),RATE(P9-1,F17,-H29)*12,IF(F5=0,(RATE(P9,F17,-J9)*12),RATE(P9,F17,-J9)*12))
        if (!(inttRate == 0 && rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance))
            cellJ10 = Utils.calCellJ10(loanAmt, toDouble(cellF17),(int)Float.parseFloat((col_P3_P10_List.get(9))));
        else
            cellJ10 = Utils.calCellJ10(loanAmt, toDouble(cellF17),(int)Float.parseFloat((col_P3_P10_List.get(9))));


        //J9
        cellJ9 = String.valueOf(loanAmt * (1 - valueOfPercentage(toFloat(subvention),1)));

    }

    public void prepareAmortTable(){
        String datePattern  = "dd/mm/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        try {
            /*************************************ROW 29 start**********************/
            Calendar cal = Calendar.getInstance();
//            cal.set(Calendar.DAY_OF_MONTH, toInt(dateOfPaymentOrDisburse.split("/")[0]));
            cal.set(Calendar.DAY_OF_MONTH, toInt(loanRepaymentRecycle));
            cal.set(Calendar.MONTH, toInt(dateOfPaymentOrDisburse.split("/")[1]));
            cal.set(Calendar.YEAR, toInt(dateOfPaymentOrDisburse.split("/")[2]));
            int day = toInt(dateOfPaymentOrDisburse.split("/")[0]);

            //C29
//        IF(F11="Advance",F12,IF(AND(F11="Arrears",DAY(F12)<20),DATE(YEAR(F12),MONTH(F12)+1,G13),DATE(YEAR(F12),MONTH(F12)+2,G13)))
            if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance) {  //advance
                col_C29_C100_List.set(29, cal.get(Calendar.DAY_OF_MONTH)+"/"+ (cal.get(Calendar.MONTH))+"/"+cal.get(Calendar.YEAR));
            }else{
                if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_arrears && day < 20){
                    cal.add(Calendar.MONTH, 1);
                    col_C29_C100_List.set(29, cal.get(Calendar.DAY_OF_MONTH)+"/"+ (cal.get(Calendar.MONTH))+"/"+cal.get(Calendar.YEAR));
                }else{
                    cal.add(Calendar.MONTH, 2);
                    col_C29_C100_List.set(29, cal.get(Calendar.DAY_OF_MONTH)+"/"+ (cal.get(Calendar.MONTH))+"/"+cal.get(Calendar.YEAR));
                }
            }

            //D29
            if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance)     col_D29_D100_List.set(29,"Advance");
            else col_D29_D100_List.set(29,"1");

            //F29
//   IF(F11="Advance",0,
// IF(D28="","",IF(J10=0,IF(P8=VLOOKUP(F9,O3:Q7,3,0),ROUND(J9*J10/P8,0)),IF(P8=VLOOKUP(F9,O3:P7,2,0),ROUND(J9*J10/P8,0)))))
            if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance)
                col_F29_F100_List.set(29,"0");
            else {
                if (toDouble(cellJ10) == 0 || cellJ10.equals("")){
                    if (col_P3_P10_List.get(8).equals(lookForElement(col_O3_O10_List, col_Q3_Q10_List, paymentInterval))){
                        col_F29_F100_List.set(29, String.valueOf(Math.round(toDouble(cellJ9) * valueOfPercentage(toDouble(cellJ10),1) / toDouble(col_P3_P10_List.get(8)))));
                    }
                }
                else {
                    if (col_P3_P10_List.get(8).equals(lookForElement(col_O3_O10_List, col_P3_P10_List, paymentInterval))){
                        col_F29_F100_List.set(29, String.valueOf(Math.round(toDouble(cellJ9) * valueOfPercentage(toDouble(cellJ10),1) / toDouble(col_P3_P10_List.get(8)))));
                    }
                }
            }

            //E29
            if (col_D29_D100_List.get(29).equals(""))
                col_E29_E100_List.set(29,"");
            else {
                if (col_P3_P10_List.get(8).equals(lookForElement(col_O3_O10_List, col_P3_P10_List, paymentInterval))){
                    col_E29_E100_List.set(29,String.valueOf(toDouble(cellF17) - toDouble(col_F29_F100_List.get(29))));
                }else
                    col_E29_E100_List.set(29,"0");
            }

            //G29
            if (col_D29_D100_List.get(29).equals("")) col_G29_G100_List.set(29,"");
            else col_G29_G100_List.set(29, String.valueOf( Math.ceil( (toDouble(col_E29_E100_List.get(29)) + Math.ceil(toDouble(col_F29_F100_List.get(29)))) ) ));

            //H29
            if (col_D29_D100_List.get(29).equals("")) col_H29_H100_List.set(29,"");
            else{
                if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance)
                    col_H29_H100_List.set(29, String.valueOf( (toDouble(cellJ9)-toDouble(col_E29_E100_List.get(29))) ));
                else col_H29_H100_List.set(29, String.valueOf( (toDouble(cellJ9)-toDouble(col_E29_E100_List.get(29))) ));
            }

            //J10
            //IF(AND(F5=0,F11="Advance"),RATE(P9-1,F17,-H29)*12,IF(F5=0,(RATE(P9,F17,-J9)*12),RATE(P9,F17,-J9)*12))
            int nPer = (int) (toDouble(col_P3_P10_List.get(9)) - 1);
            if ((inttRate == 0 && rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance))
                cellJ10 = Utils.calCellJ10(toDouble(col_H29_H100_List.get(29)), toDouble(cellF17), nPer);


            /******************************************ROW 29 END*********************************************************************/



            /****************************************ROW 30 START*********************************************/

            //C30
//            IF(F11="Arrears",EDATE(C29,VLOOKUP(F9,O3:Q7,3,0)),
//                IF(D30="","",IF(F15="Next Month",DATE(YEAR(C29),MONTH(C29)+1,G13),DATE(YEAR(C29),MONTH(C29)+2,G13))))
            Calendar cal30 = Calendar.getInstance();
//            cal30.set(Calendar.DAY_OF_MONTH, toInt(col_C29_C100_List.get(29).split("/")[0]) );
            cal30.set(Calendar.DAY_OF_MONTH, toInt(loanRepaymentRecycle) );
            cal30.set(Calendar.MONTH , toInt(col_C29_C100_List.get(29).split("/")[1]) );
            cal30.set(Calendar.YEAR, toInt(col_C29_C100_List.get(29).split("/")[2]));

            if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_arrears){
                int nxtMonth = toInt(lookForElement(col_O3_O10_List, col_Q3_Q10_List, paymentInterval));
                cal30.add(Calendar.MONTH, nxtMonth);
            }else {
                if(toInt(dateOfPaymentOrDisburse.split("/")[0]) < 21){
                    cal30.add(Calendar.MONTH, 1);
                }else{
                    cal30.add(Calendar.MONTH, 2);
                }
            }
            col_C29_C100_List.set(30, cal30.get(Calendar.DAY_OF_MONTH)+"/"+cal30.get(Calendar.MONTH)+"/"+cal30.get(Calendar.YEAR));

            //D30
            if (col_D29_D100_List.get(29).equals(""))
                col_D29_D100_List.set(30,"");
            else
                col_D29_D100_List.set(30,"2");


            //F30
//            IF(J10=0,IF(D30="","",IF(F11="Advance",IF(P8=VLOOKUP(F9,O3:Q7,3,0),H29*J10/P8,0),IF(VLOOKUP(F9,O3:Q7,3,0),H29*J10/P8,0))),
//            IF(D30="","",IF(F11="Advance",IF(OR(P8=P3,P8=P4),H29*J10/P8,0),IF(OR(P8=P3,P8=P4),H29*J10/P8,0))))
            if (toDouble(cellJ10) <= 0){
                if (col_D29_D100_List.get(30).equals("")){
                    col_F29_F100_List.set(30,"");
                }else{
                    if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance){ // advance
                        if (col_P3_P10_List.get(8).equals(lookForElement(col_O3_O10_List, col_Q3_Q10_List, paymentInterval))){
                           col_F29_F100_List.set(30,String.valueOf( (toDouble(col_H29_H100_List.get(29)) * valueOfPercentage(toDouble(cellJ10),1) / toDouble(col_P3_P10_List.get(8)))));
                        }
                        else {
                            col_F29_F100_List.set(30,"0");
                        }
                    }else {   //Arrears
                        if (!(lookForElement(col_O3_O10_List, col_Q3_Q10_List, paymentInterval).equals("")) ){
                            col_F29_F100_List.set(30,String.valueOf( ( (toDouble(col_H29_H100_List.get(29)) * valueOfPercentage(toDouble(cellJ10),1) / toDouble(col_P3_P10_List.get(8))))));
                        }
                        else {
                            col_F29_F100_List.set(30,"0");
                        }
                    }
                }
            }
            else{
                if (col_D29_D100_List.get(30).equals("")){
                    col_F29_F100_List.set(30,"");
                }else {
                    if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance){  //Advance
                        if (col_P3_P10_List.get(8).equals(col_P3_P10_List.get(3)) || col_P3_P10_List.get(8).equals(col_P3_P10_List.get(4))){
                            col_F29_F100_List.set(30,String.valueOf( (toDouble(col_H29_H100_List.get(29)) * valueOfPercentage(toDouble(cellJ10),1) / toDouble(col_P3_P10_List.get(8)))));
                        }
                        else {
                            col_F29_F100_List.set(30,"0");
                        }
                    }else{  //Arrears
                        if (col_P3_P10_List.get(8).equals(col_P3_P10_List.get(3)) || col_P3_P10_List.get(8).equals(col_P3_P10_List.get(4))){
                            col_F29_F100_List.set(30,String.valueOf( (toDouble(col_H29_H100_List.get(29)) * valueOfPercentage(toDouble(cellJ10),1) / toDouble(col_P3_P10_List.get(8)))));
                        }
                        else {
                            col_F29_F100_List.set(30,"0");
                        }
                    }
                }
            }

            //E30
            if (col_D29_D100_List.get(30).equals(""))
                col_E29_E100_List.set(30, "");
            else if (col_P3_P10_List.get(8).equals(lookForElement(col_O3_O10_List, col_P3_P10_List,paymentInterval))){
                col_E29_E100_List.set(30,String.valueOf( toDouble(cellF17) - toDouble(col_F29_F100_List.get(30)) ));
            }else
                col_E29_E100_List.set(30, "0");


            //G30
            if (col_D29_D100_List.get(30).equals(""))
                col_G29_G100_List.set(30, "");
            else{
                double E30 = toDouble(col_E29_E100_List.get(30));
                double F30 = toDouble(col_F29_F100_List.get(30));
                col_G29_G100_List.set(30, String.valueOf(Math.ceil((E30 + F30))));
            }

            //H30
            if (col_D29_D100_List.get(30).equals(""))
                col_H29_H100_List.set(30, "");
            else {
                col_H29_H100_List.set(30, String.valueOf(toDouble(col_H29_H100_List.get(29)) - toDouble(col_E29_E100_List.get(30))));
            }

            /******************************************ROW 30 END*********************************************************************/

            /**************************************ROW 31 & ONWARDS*****************************/

            //            cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_MONTH, toInt(col_C29_C100_List.get(30).split("/")[0]));
            cal.set(Calendar.MONTH, toInt(col_C29_C100_List.get(30).split("/")[1]));
            cal.set(Calendar.YEAR, toInt(col_C29_C100_List.get(30).split("/")[2]));

            for (int i = 31; (i-31) < (toFloat(col_P3_P10_List.get(9))-2); i++){
                //C31+
                cal.add(Calendar.MONTH, toInt(lookForElement(col_O3_O10_List, col_Q3_Q10_List, paymentInterval)));
                if (cal.get(Calendar.MONTH ) != 0)
                    col_C29_C100_List.set(i, cal.get(Calendar.DAY_OF_MONTH) +"/"+ cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR));
                else
                    col_C29_C100_List.set(i, cal.get(Calendar.DAY_OF_MONTH) +"/"+ 12 +"/"+(cal.get(Calendar.YEAR)-1));

                //D31+
                col_D29_D100_List.set(i, String.valueOf(toInt(col_D29_D100_List.get(i-1) )+ 1) );

                //F31+
//                IF(J10=0,
//                   IF(D31="",
//                     "",IF(F11="Advance",IF(OR(P8=P3,P8=P4),H30*J10/P8,0),IF(OR(P8=P3,P8=P4),H30*J10/P8,0))),IF(D31="","",IF(F11="Advance",IF(OR(P8=P3,P8=P4),H30*J10/P8,0),IF(OR(P8=P3,P8=P4),H30*J10/P8,0))))
                double J10 = valueOfPercentage(toDouble(cellJ10),1);
                double P8 = toDouble(col_P3_P10_List.get(8));
                double P3 = toDouble(col_P3_P10_List.get(3));
                double P4 = toDouble(col_P3_P10_List.get(4));
                double H_iMinusOne = toDouble(col_H29_H100_List.get(i-1));

                if (cellJ10.equals("") || toDouble(cellJ10) == 0) {
                    if (col_D29_D100_List.get(i).equals(""))
                        col_F29_F100_List.set(i, "");
                    else{
                        if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance){
                            if ( (P8 == P3) || (P8 == P4) ){
                                col_F29_F100_List.set(i, String.valueOf(H_iMinusOne * J10/P8));
                            }else
                                col_F29_F100_List.set(i, "0");
                        }else{
                            if ( (P8 == P3) || (P8 == P4) ){
                                col_F29_F100_List.set(i, String.valueOf(H_iMinusOne * J10/P8));
                            }else
                                col_F29_F100_List.set(i, "0");
                        }
                    }
                }else{
                    if (col_D29_D100_List.get(i).equals(""))
                        col_F29_F100_List.set(i, "");
                    else{
                        if (rg_paymnt_mode.getCheckedRadioButtonId() == R.id.rb_advance){
                            if ( (P8 == P3) || (P8 == P4) ){
                                col_F29_F100_List.set(i, String.valueOf(H_iMinusOne * J10/P8));
                            }else
                                col_F29_F100_List.set(i, "0");
                        }else{
                            if ( (P8 == P3) || (P8 == P4) ){
                                col_F29_F100_List.set(i, String.valueOf(H_iMinusOne * J10/P8));
                            }else
                                col_F29_F100_List.set(i, "0");
                        }
                    }
                }



                //E31+
                if (col_D29_D100_List.get(i).equals(""))
                    col_E29_E100_List.set(i, "");
                else {
                    if (col_P3_P10_List.get(8).equals(lookForElement(col_O3_O10_List, col_P3_P10_List, paymentInterval))){
                        col_E29_E100_List.set(i,String.valueOf( toDouble(cellF17) - toDouble(col_F29_F100_List.get(i)) ));
                    }else
                        col_E29_E100_List.set(i, "0");
                }

                //G31+
                if (col_D29_D100_List.get(i).equals(""))
                    col_G29_G100_List.set(i,"");
                else{
                    col_G29_G100_List.set(i, String.valueOf(Math.ceil( (toDouble(col_E29_E100_List.get(i))) + (toDouble(col_F29_F100_List.get(i))) ) ));
                }

                //H31+
                if (col_D29_D100_List.get(i).equals(""))
                    col_H29_H100_List.set(i,"");
                else {
                    col_H29_H100_List.set(i, String.valueOf(H_iMinusOne - toDouble(col_E29_E100_List.get(i))));
                }

                Log.d("END","dsjldkas;");
//
            } //for

//            prepareTableView();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void prepareTableView() {
        mColumnHeaderList = new ArrayList<>();
        mRowHeaderList = new ArrayList<>();
        mCellList = new ArrayList<>();
        ColumnHeader columnHeader = new ColumnHeader();
        RowHeader rowHeader = new RowHeader();
        Cell cell = new Cell();

        columnHeader.setColumnHeader("Date");
        mColumnHeaderList.add(columnHeader);

        columnHeader = new ColumnHeader();
        columnHeader.setColumnHeader("EMI");
        mColumnHeaderList.add(columnHeader);

        columnHeader = new ColumnHeader();
        columnHeader.setColumnHeader("Principal");
        mColumnHeaderList.add(columnHeader);

        columnHeader = new ColumnHeader();
        columnHeader.setColumnHeader("Interest");
        mColumnHeaderList.add(columnHeader);

        columnHeader = new ColumnHeader();
        columnHeader.setColumnHeader("Balance");
        mColumnHeaderList.add(columnHeader);

        for (int i=29; (i-29)<toDouble(col_P3_P10_List.get(9)); i++){
            rowHeader = new RowHeader();
            rowHeader.setRowHeader(col_D29_D100_List.get(i));
//            rowHeader.setRowHeader(col_C29_C100_List.get(i));
            mRowHeaderList.add(rowHeader);

            List<Cell> cells2 = new ArrayList<>();
            cell = new Cell();
            cell.setCellValue(col_C29_C100_List.get(i));
            cells2.add(cell);

            cell = new Cell();
            cell.setCellValue(col_G29_G100_List.get(i));
            cells2.add(cell);

            cell = new Cell();
            cell.setCellValue(String.valueOf(Math.round(toDouble(col_E29_E100_List.get(i)))));
            cells2.add(cell);

            cell = new Cell();
            cell.setCellValue(String.valueOf(Math.round(toDouble(col_F29_F100_List.get(i)))));
            cells2.add(cell);

            cell = new Cell();
            cell.setCellValue(String.valueOf(Math.round(toDouble(col_H29_H100_List.get(i)))));
            cells2.add(cell);

            mCellList.add(cells2);
        }

        MyTableViewAdapter adapter = new MyTableViewAdapter(getApplicationContext());
        tableView.setAdapter(adapter);
        tableView.setRowHeaderWidth(160);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tableView.getRowHeaderRecyclerView().setBackgroundColor(getColor(R.color.grey2));
            tableView.getRowHeaderRecyclerView().setElevation(12);
        }
        adapter.setAllItems(mColumnHeaderList,mRowHeaderList,mCellList);

    }

    public String lookForElement(ArrayList<String> list1, ArrayList<String> list2, String key ){
        String element = "";
        for (int i=0; i<list1.size(); i++){
            if (key.equalsIgnoreCase(list1.get(i))){
                element = list2.get(i);
                break;
            }
        }
        return element;
    }


}
