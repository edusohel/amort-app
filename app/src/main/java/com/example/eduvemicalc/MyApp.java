package com.example.eduvemicalc;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.example.eduvemicalc.utils.FontsOverride;

import io.fabric.sdk.android.Fabric;

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/ubuntu_regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/ubuntu_regular.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/ubuntu_regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS", "fonts/ubuntu_regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/ubuntu_regular.ttf");
    }
}
