package com.example.eduvemicalc.utils;

import android.util.Log;
import java.util.ArrayList;

public class Utils {

    public static float toFloat(String v){
        return Float.parseFloat(v);
    }
    public static int toInt(String v){
        return  Integer.parseInt(v);
    }
    public static double toDouble(String v){
        return Double.parseDouble(v);
    }

    public static double valueOfPercentage(double percent, double value){
        return ((percent/100)*value);
    }

    public static void fillEmptyList(ArrayList<String> list, int startIndex, int endIndex){
        for (int i= startIndex; i<= endIndex; i++){
            list.add(i,"");
        }
    }


    public static String calCellJ10(double LA,double EMI ,int nPeriod){
//        double LA = 100000;
//        double EMI = 5675;
        float i = 0.01f;

        double Effective_Return_Rate = parth(i, nPeriod, EMI, (-LA)) *12;
//        Double Effective_Return_Rate = Double.valueOf(String.format("%.4f",(Double.valueOf(parth(i, nPeriod, EMI, (-LA))))*12));

        Log.d("abs1"," ABS1 : "+Effective_Return_Rate);
        return String.valueOf(Math.ceil(Effective_Return_Rate*100));  //return percentage
//        while (i < 100.0){
//            double expo = Math.pow((1+i), (-nPeriod));
//            double ki = (1-expo)/i;
//            ki=ki*12;
//            Log.d("KKK","ki - "+ki+" i - "+ i);
//          Double z = Double.valueOf(String.format("%.6f", (k)));
////          Double abs = Double.valueOf(String.format("%.5f",Double.valueOf(rate(12,EMI,LA))));
//          Double abs1 = Double.valueOf(String.format("%.4f",(Double.valueOf(parth(i,nPeriod,EMI,(-LA))))*12));
//            i = i + 0.01f;
////             if(z == abs){
////                 Log.d("IFF","If LA/EMI : "+z +" ABS : "+abs +" I : "+i);
////             }
////             else{
////                 Log.d("Else","Else LA/EMI : "+z +" ABS : "+abs +" I : "+i);
////             }
//
//            if(z == abs1){
//                Log.d("IFF1","If LA/EMI : "+z +" ABS1 : "+abs1 +" I : "+i);
//            }
//            else{
//                Log.d("Else1","Else LA/EMI : "+z +" ABS1 : "+abs1 +" I : "+i);
//            }
//        }
    }

//    public static double rate(double nper, double pmt, double pv)
//    {
//        double error = 0.0000001;
//        double high =  1.00;
//        double low = 0.00;
//
//        double rate = (2.0 * (nper * pmt - pv)) / (pv * nper);
//
//        while(true) {
//            // check for error margin
//            double calc = Math.pow(1 + rate, nper);
//            calc = (rate * calc) / (calc - 1.0);
//            calc -= pmt / pv;
//
//            if (calc > error) {
//                // guess too high, lower the guess
//                high = rate;
//                rate = (high + low) / 2;
//            } else if (calc < -error) {
//                // guess too low, higher the guess
//                low = rate;
//                rate = (high + low) / 2;
//            } else {
//                // acceptable guess
//                break;
//            }
//        }
//
//        System.out.println("Rate : "+rate);
//        return rate;
//    }

//    https://gist.github.com/kucukharf/677d8f21660efaa33f72
    public static double parth(double guess, int periods,double payment,double present ){
//=RATE(12,8448,(-100000))*12
//        double guess = 0.01;
//        int periods =12;
//        double payment = 8448;
//        double present = 100000;
        double future = 0.0;

        int type = 0;

        // Set maximum epsilon for end of iteration
        double epsMax = 1E-10;
//        double epsMax = 0.0000001;
        Log.d("UTIL","epsMax - "+epsMax);

        // Set maximum number of iterations
        double iterMax = 10;

        // Implement Newton's method
        double y, y0, y1, x0, x1 = 0,
                f = 0,
                i = 0;
        double rate = guess;

        if (Math.abs(rate) < epsMax) {
            y = present * (1 + periods * rate) + payment * (1 + rate * type) * periods + future;
        } else {
            f = Math.exp(periods * Math.log(1 + rate));
            y = present * f + payment * (1 / rate + type) * (f - 1) + future;
        }
        y0 = present + payment * periods + future;
        y1 = present * f + payment * (1 / rate + type) * (f - 1) + future;
        i = x0 = 0;
        x1 = rate;
        while ((Math.abs(y0 - y1) > epsMax) && (i < iterMax)) {
            rate = (y1 * x0 - y0 * x1) / (y1 - y0);
            x0 = x1;
            x1 = rate;
            if (Math.abs(rate) < epsMax) {
                y = present * (1 + periods * rate) + payment * (1 + rate * type) * periods + future;
            } else {
                f = Math.exp(periods * Math.log(1 + rate));
                y = present * f + payment * (1 / rate + type) * (f - 1) + future;
            }
            y0 = y1;
            y1 = y;
            ++i;
        }
        Log.d("UTIL","PARTHRATE - "+rate);
        return rate;
    }

}
