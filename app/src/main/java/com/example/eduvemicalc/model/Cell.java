package com.example.eduvemicalc.model;

public class Cell {
    String cellValue="";

    public String getCellValue() {
        return cellValue;
    }

    public void setCellValue(String cellValue) {
        this.cellValue = cellValue;
    }
}
