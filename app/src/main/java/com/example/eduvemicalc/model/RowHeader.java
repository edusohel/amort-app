package com.example.eduvemicalc.model;

public class RowHeader {
    String rowHeader ="";

    public String getRowHeader() {
        return rowHeader;
    }

    public void setRowHeader(String rowHeader) {
        this.rowHeader = rowHeader;
    }
}
