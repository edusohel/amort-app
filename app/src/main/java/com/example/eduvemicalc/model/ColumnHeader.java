package com.example.eduvemicalc.model;

public class ColumnHeader {
    String columnHeader="";

    public String getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }
}
